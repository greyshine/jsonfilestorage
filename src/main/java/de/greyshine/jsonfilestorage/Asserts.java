package de.greyshine.jsonfilestorage;

public abstract class Asserts {

    private Asserts() {
    }

    public static void notNull(Object o) {
        notNull(o, null);
    }

    public static void notNull(Object o, String message) {
        if (o != null) {
            return;
        }
        throw new IllegalStateException(message == null ? "object is null" : message);
    }

    public static void isTrue(boolean b) {
        isTrue(b, null);
    }

    public static void isTrue(boolean b, String message) {
        if (b) {
            return;
        }
        throw new IllegalStateException(message == null ? "boolean is false" : message);
    }

    public static void isFalse(boolean b) {
        isFalse(b, null);
    }

    public static void isFalse(boolean b, String message) {
        if (!b) {
            return;
        }
        throw new IllegalStateException(message != null ? message : "boolean is true");
    }

    public static void isNull(Object object) {
        isNull(object, null);
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            return;
        }
        throw new IllegalStateException(message != null ? message : "object is not null: " + object);
    }

    public static void isEqual(Object a, Object b) {

    }

    public static void isEqual(Object a, Object b, String message) {

        if (a != null && a.equals(b)) {
            return;
        }
        throw new IllegalStateException(message != null ? message : "objects are not equal: " + a + ", " + b);
    }
}
