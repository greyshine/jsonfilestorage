package de.greyshine.jsonfilestorage;

import java.util.List;
import java.util.function.Function;

public interface JsonFileStorage {

    String create(Object entity);

    <T> T create(Class<T> entityClass, String id);

    <T> T read(Class<T> clazz, String id);

    <T> void update(T entity);

    <T> boolean delete(Class<T> entityClass, String id);

    <T> boolean delete(T entity);

    String toString(Object object);

    String upsert(Object entity);

    <T> void handle(Class<T> entityClass, String id, Function<T, HandleResult> handler);

    /**
     * @param clazz            the type being handled
     * @param deciderFunction  function which decides on selection of the 'current' entry to be handled
     * @param consumerFunction handling a taken/decided on entry
     * @param <T>              en entity with an @{Id}
     */
    <T> void handle(Class<T> clazz, Function<T, Boolean> deciderFunction, Function<T, HandleResult> consumerFunction);

    <T> List<T> list(Class<T> clazz, Function<T, Boolean> deciderFunction);

    enum HandleResult {
        SAVE,
        DELETE,
        NOTHING
    }
}
