package de.greyshine.jsonfilestorage;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

@Slf4j
public class JsonFileStorageImpl implements JsonFileStorage {

    private final static Object SYNC = new Object();
    final File basedir;
    private final ObjectMapper mapper = new ObjectMapper();
    private final ObjectWriter jsonWriter = mapper.writerWithDefaultPrettyPrinter();

    private final Map<CacheId<?>, CacheValue<?>> cache = new HashMap<>();

    private final Statistics statistics = new Statistics();

    public JsonFileStorageImpl(File basedir) {

        Asserts.notNull(basedir, "basedir is null");
        Asserts.isTrue(basedir.getAbsoluteFile().isDirectory(), "basedir must be a directory");

        this.basedir = basedir.getAbsoluteFile();
    }

    @SneakyThrows
    @Override
    public <T> T create(Class<T> entityClass, String id) {

        // check id is null or no entity can be retrieved with demanded id
        Asserts.isTrue(id == null || read(entityClass, id) == null, "ID is already taken: " + id);

        final T object = Utils.instantiate(entityClass, id == null);

        create(object);

        return object;
    }

    @Override
    public String create(Object entity) {

        final long st = System.currentTimeMillis();

        synchronized (SYNC) {

            final String id = Utils.initId(entity);

            final File file = Utils.getFile(basedir, entity.getClass(), id);

            file.getParentFile().mkdirs();
            Asserts.isTrue(file.getParentFile().isDirectory(), "no directory for file: " + file);
            Asserts.isFalse(file.exists(), "file already exists: " + file);

            try {
                jsonWriter.writeValue(file, entity);
            } catch (IOException exception) {
                throw new RuntimeException(exception);
            }

            cache.put(new CacheId(entity.getClass(), id), new CacheValue<>(entity));

            statistics.timeCreate.addAndGet(System.currentTimeMillis() - st);
            statistics.countCreate.addAndGet(1);

            return id;
        }
    }

    @Override
    public String upsert(Object entity) {

        Asserts.notNull(entity, "object must be provided");

        String id = Utils.getId(entity);

        if (id == null) {
            id = create(entity);
        } else {
            update(entity);
        }

        return id;
    }

    private <T> T getCachedObject(Class<T> clazz, String id) {

        if (id == null || id.isBlank()) {
            return null;
        }

        final CacheId<T> cacheId = new CacheId(clazz, id);

        synchronized (cache) {

            final CacheValue<T> objectWrapper = (CacheValue<T>) cache.get(cacheId);

            if (objectWrapper == null) {
                return null;
            }

            objectWrapper.usages++;

            return objectWrapper.object;
        }
    }

    @Override
    public <T> T read(Class<T> clazz, String id) {

        final long st = System.currentTimeMillis();

        Asserts.notNull(clazz, "class must be provided");
        Asserts.notNull(id, "id must be provided");

        T result = getCachedObject(clazz, id);
        if (result != null) {
            return result;
        }

        final File file = Utils.getFile(basedir, clazz, id);

        if (!file.exists()) {
            return null;
        }

        synchronized (cache) {

            try {
                result = mapper.readValue(file, clazz);
            } catch (IOException exception) {
                throw new RuntimeException(exception);
            }

            cache.put(new CacheId(clazz, id), new CacheValue<>(result));
        }

        statistics.timeRead.addAndGet(System.currentTimeMillis() - st);
        statistics.countRead.addAndGet(1);
        return result;
    }

    @Override
    public <T> void update(T entity) {

        final long st = System.currentTimeMillis();

        final String id = Utils.getId(entity);
        Asserts.notNull(id, "id must be provided");

        final File file = Utils.getFile(basedir, entity.getClass(), id);

        Asserts.isTrue(file.isFile(), "file does not exist: " + file);
        Asserts.isTrue(file.canRead(), "file cannot be accessed");

        synchronized (SYNC) {

            try {
                jsonWriter.writeValue(file, entity);
            } catch (IOException exception) {
                throw new RuntimeException(exception);
            }
        }

        statistics.timeUpdate.addAndGet(System.currentTimeMillis() - st);
        statistics.countUpdate.addAndGet(1);
    }

    @Override
    public <T> boolean delete(T entity) {

        Asserts.notNull(entity, "Class must be provided");

        final String id = Utils.getId(entity);

        final boolean deleted = delete(entity.getClass(), id);

        if (deleted) {
            Utils.setIdNull(entity);
        }

        return deleted;
    }

    @Override
    public <T> boolean delete(Class<T> entityClass, String id) {

        final long st = System.currentTimeMillis();

        Asserts.notNull(entityClass, "Class must be provided");
        Asserts.notNull(id, "id must be provided");

        final CacheId<T> cacheId = new CacheId(entityClass, id);
        final File file = Utils.getFile(basedir, entityClass, id);

        synchronized (SYNC) {

            cache.remove(cacheId);

            if (!file.isFile()) {
                return false;
            }

            file.delete();
        }

        statistics.timeDelete.addAndGet(System.currentTimeMillis() - st);
        statistics.countDelete.addAndGet(1);

        return !file.exists();
    }

    @Override
    @SneakyThrows
    public String toString(Object object) {

        if (object == null) {
            return null;
        } else if (object.getClass().isPrimitive()) {
            return String.valueOf(object);
        }

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        jsonWriter.writeValue(baos, object);
        return baos.toString(StandardCharsets.UTF_8);
    }

    @Override
    public <T> List<T> list(Class<T> entityClass, Function<T, Boolean> deciderFunction) {

        final List<T> items = new ArrayList<>();
        if (deciderFunction == null) {
            return items;
        }

        final Function<T, HandleResult> function = jsonObject -> {
            items.add(jsonObject);
            return HandleResult.NOTHING;
        };

        handle(entityClass, deciderFunction, function);

        return items;
    }

    @Override
    public <T> void handle(final Class<T> entityClass, final Function<T, Boolean> deciderFunction, final Function<T, HandleResult> consumerFunction) {

        Asserts.notNull(entityClass, "entity class must not be null");
        Asserts.notNull(deciderFunction, "decider function must not be null");
        Asserts.notNull(consumerFunction, "consumer function must not be null");

        final File classesDir = new File(basedir + File.separator + entityClass.getCanonicalName());
        All:
        for (File dir : classesDir.listFiles(File::isDirectory)) {
            for (File jsonFile : dir.listFiles(file -> file.isFile() && file.getName().toLowerCase(Locale.ROOT).endsWith(".json"))) {

                try {

                    final T jsonObject = mapper.readValue(jsonFile, entityClass);
                    final Boolean deciderResult = deciderFunction.apply(jsonObject);

                    // received stop indicator
                    if (deciderResult == null) {
                        break All;
                    } else if (!deciderResult) {
                        continue;
                    }

                    final HandleResult handleResult = consumerFunction.apply(jsonObject);

                    if (HandleResult.SAVE == handleResult) {
                        update(jsonObject);
                    } else if (HandleResult.DELETE == handleResult) {
                        delete(jsonObject);
                    }

                } catch (IOException exception) {
                    throw new RuntimeException(exception);
                }
            }
        }
    }

    @Override
    public <T> void handle(Class<T> entityClass, String id, Function<T, HandleResult> handler) {

        Asserts.notNull(entityClass, "No entity class given");
        Asserts.isTrue(id != null && !id.isBlank(), "No id given");
        Asserts.notNull(handler, "No handler given");

        synchronized (SYNC) {

            final T jsonObject = read(entityClass, id);

            final HandleResult handleResult = handler.apply(jsonObject);

            if (handleResult == null) {
                return;
            }

            switch (handleResult) {

                case SAVE:
                    upsert(jsonObject);
                    break;

                case DELETE:
                    delete(jsonObject);
                    break;

                case NOTHING:
                default:
                    return;
            }
        }
    }

    public void clearCache() {
        synchronized (SYNC) {
            cache.clear();
        }
    }

    @EqualsAndHashCode
    @ToString
    private static class CacheId<T> {

        final Class<T> entityClass;
        final String id;

        CacheId(Class<T> entityClass, String id) {
            this.entityClass = entityClass;
            this.id = id;
        }
    }

    @ToString
    private static class CacheValue<T> {

        final T object;
        final long created = System.currentTimeMillis();
        long usages = 0;

        CacheValue(T object) {
            this.object = object;
        }
    }

    @Getter
    public static class Statistics {

        private final AtomicLong countCreate = new AtomicLong(0);
        private final AtomicLong countRead = new AtomicLong(0);
        private final AtomicLong countUpdate = new AtomicLong(0);
        private final AtomicLong countDelete = new AtomicLong(0);

        private final AtomicLong timeCreate = new AtomicLong(0);
        private final AtomicLong timeRead = new AtomicLong(0);
        private final AtomicLong timeUpdate = new AtomicLong(0);
        private final AtomicLong timeDelete = new AtomicLong(0);
    }
}
