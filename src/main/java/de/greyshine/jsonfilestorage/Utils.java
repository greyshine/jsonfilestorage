package de.greyshine.jsonfilestorage;

import lombok.SneakyThrows;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

public abstract class Utils {

    private final static Map<Class<?>, Field> idFields = new HashMap<>();

    private Utils() {
    }

    public static String getAbbreviation(String id) {

        Asserts.notNull(id);
        Asserts.isFalse(id.isBlank(), "id is blank");

        int i = 0;

        for (byte b : id.getBytes(StandardCharsets.UTF_8)) {

            i += b;
            i = i >= 0 ? i : i * -1;

            if (Integer.toHexString(i).length() > 6) {
                i = b;
            }
        }

        return Integer.toHexString(i).toLowerCase(Locale.ROOT);
    }

    @SneakyThrows
    public static String initId(Object entity) {

        final Field field = getIdField(entity);

        Asserts.notNull(field, "no String field found with @Id");

        String id = (String) field.get(entity);

        if (id != null && !id.isBlank()) {
            return id;
        }

        id = UUID.randomUUID().toString();
        field.set(entity, id);

        return id;
    }

    public static File getFile(File basedir, Class<?> entityClass, String id) {

        Asserts.isTrue(basedir != null && basedir.isDirectory(), "basedir is not accessible");
        Asserts.notNull(entityClass, "entity class must not be null");
        Asserts.isTrue(id == null || !id.isBlank(), "given id is blank");

        final String subDir = entityClass.getCanonicalName() + File.separator + Utils.getAbbreviation(id);
        return new File(basedir + File.separator + subDir, id + ".json").getAbsoluteFile();
    }

    @SneakyThrows
    public static String getId(Object entity) {
        final Field field = getIdField(entity);
        return (String) field.get(entity);
    }

    @SneakyThrows
    public static void setIdNull(Object entity) {
        final Field field = getIdField(entity);
        field.set(entity, null);
    }

    public static Field getIdField(Object jsonEntity) {

        Asserts.notNull(jsonEntity, "");

        Field idField = idFields.get(jsonEntity.getClass());

        if (idField == null) {

            for (Field field : jsonEntity.getClass().getDeclaredFields()) {

                if (field.getAnnotation(Id.class) == null) {
                    continue;
                } else if (idField != null) {
                    throw new IllegalStateException("Property with @" + Id.class.getCanonicalName() + " only allowed once");
                } else if (field.getType() != String.class) {
                    throw new IllegalStateException("Property with @" + Id.class.getCanonicalName() + " must be of type String");
                }

                final int modifiers = field.getModifiers();

                if (Modifier.isFinal(modifiers)) {
                    throw new IllegalStateException("Property must not be final");
                } else if (Modifier.isStatic(modifiers)) {
                    throw new IllegalStateException("Property must not be static");
                }

                idField = field;
            }

            Asserts.notNull(idField, "no field with @" + Id.class.getCanonicalName() + " found");
            idField.setAccessible(true);
        }

        return idField;
    }

    public static boolean isProperDirectory(File file) {

        if (file == null) {
            return false;
        } else if (!file.exists()) {
            return false;
        } else if (!file.isDirectory()) {
            return false;
        } else return file.getPath().equals(file.getAbsolutePath());
    }

    @SneakyThrows
    public static <T> T instantiate(Class<T> entityClass, boolean initId) {

        Asserts.notNull(entityClass, "no entity class provided");

        final T jsonObject = entityClass.getDeclaredConstructor().newInstance();

        final Field idField = getIdField(jsonObject);
        idField.set(jsonObject, !initId ? null : UUID.randomUUID().toString());

        return jsonObject;
    }
}
