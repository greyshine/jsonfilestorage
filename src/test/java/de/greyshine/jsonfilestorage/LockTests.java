package de.greyshine.jsonfilestorage;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Slf4j
public class LockTests {

    private JsonFileStorage jsf;

    @Test
    public void test() {

        final Runnable r1 = () -> {
            System.out.println("STARTED1");
            System.out.println("ENDED1");
        };

        final Runnable r2 = () -> {
            System.out.println("STARTED2");
            System.out.println("ENDED2");
        };

        final List<Runner> runners = new ArrayList<>();
        runners.add(new Runner(r1));
        runners.add(new Runner(r2));
        final Set<Thread> ts = new HashSet<>();

        System.out.println("all started");

        while (!ts.isEmpty()) {

            final Thread t = ts.iterator().next();
            try {
                t.join();
                ts.remove(t);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        System.out.println("all ended");
    }

    @BeforeEach
    public void before() throws IOException {

        final File dir = new File("target/test-dir");

        FileUtils.deleteDirectory(dir);
        Assertions.assertTrue(dir.mkdirs());

        jsf = new JsonFileStorageImpl(dir);
    }

    final class Runner implements Runnable {

        static final Set<Runner> runners = new LinkedHashSet<>();

        static int cnt = 0;

        final Thread thread;

        final int no = cnt++;
        final Runnable runnable;
        boolean started = false;

        Runner(Runnable r) {
            runnable = r;
            thread = new Thread(this);
            thread.setName("[" + cnt + "] " + r.toString());
            thread.setDaemon(true);
            runners.add(this);
        }

        public void start() {
            thread.start();
        }

        @Override
        public void run() {

            this.started = true;

            log.debug("started: {}", thread);

            try {
                runnable.run();
            } catch (Throwable t) {
                throw t;
            } finally {
                log.debug("finished: {}", thread);
            }
        }

        void join() throws InterruptedException {
            this.thread.join();
        }
    }

}
