package de.greyshine.jsonfilestorage;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;

@Slf4j
public class CrudTests {

    private JsonFileStorage jsf;

    @BeforeEach
    public void before() throws IOException {

        log.info("Starting test...");

        final File dir = new File("target/test-dir");

        FileUtils.deleteDirectory(dir);
        Assertions.assertTrue(dir.mkdirs());

        jsf = new JsonFileStorageImpl(dir);
    }

    @Test
    public void testCrud() {

        final TestEntity testEntity = new TestEntity();

        final String id = jsf.create(testEntity);
        final TestEntity testEntity2 = jsf.read(TestEntity.class, id);

        Assertions.assertEquals(testEntity.getId(), testEntity2.getId());
        Assertions.assertEquals(testEntity, testEntity2);

        final boolean deleted = jsf.delete(TestEntity.class, id);
        Assertions.assertTrue(deleted);

        final TestEntity testEntity3 = jsf.read(TestEntity.class, id);
        Assertions.assertNull(testEntity3);

        System.out.println("Test success!");
    }

    @Data
    public static class TestEntity {

        public final String[] texts = new String[]{"a-value", "b-value"};
        public String test;
        public BigDecimal number;
        public boolean bool;
        @Id
        private String id;

        private SubEntity subEntity = SubEntity.builder().name("a-name").build();
    }

    @Builder
    @Data
    private static class SubEntity {
        private String name;
    }
}